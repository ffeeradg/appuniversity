class Department < ApplicationRecord
  has_many :teachers, dependent: :destroy
  has_many :courses, dependent: :destroy
end
