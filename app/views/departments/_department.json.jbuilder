json.extract! department, :id, :codigo, :nombre, :created_at, :updated_at
json.url department_url(department, format: :json)
