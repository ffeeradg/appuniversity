json.extract! student, :id, :codigo, :nombre, :direccion, :telefono, :created_at, :updated_at
json.url student_url(student, format: :json)
