json.extract! teacher, :id, :codigo, :nombre, :direccion, :telefono, :titulo_profesional, :nivel_estudios, :created_at, :updated_at
json.url teacher_url(teacher, format: :json)
