json.extract! university, :id, :codigo, :nombre, :direccion, :telefono, :codigo_postal, :created_at, :updated_at
json.url university_url(university, format: :json)
