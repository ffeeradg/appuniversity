class CreateUniversities < ActiveRecord::Migration[5.1]
  def change
    create_table :universities do |t|
      t.string :codigo
      t.string :nombre
      t.string :direccion
      t.string :telefono
      t.string :codigo_postal

      t.timestamps
    end
  end
end
