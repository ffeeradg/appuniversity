class CreateStudents < ActiveRecord::Migration[5.1]
  def change
    create_table :students do |t|
      t.string :codigo
      t.string :nombre
      t.string :direccion
      t.string :telefono

      t.timestamps
    end
  end
end
