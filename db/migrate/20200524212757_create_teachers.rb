class CreateTeachers < ActiveRecord::Migration[5.1]
  def change
    create_table :teachers do |t|
      t.string :codigo
      t.string :nombre
      t.string :direccion
      t.string :telefono
      t.string :titulo_profesional
      t.string :nivel_estudios

      t.timestamps
    end
  end
end
